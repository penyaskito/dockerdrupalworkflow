#!/bin/bash
set -eux
./download_drupal.sh
./create_dev_volumes.sh
docker-compose up -d
sleep 10
./install_drupal.sh
echo "Enter to http://localhost/"
