#!/bin/bash
source .env
docker-compose down
docker images prune -a
docker volume prune -f
sudo rm -rf volumes-$ENV_STAGE ./drupal
sudo rm -rf drupal_etc/settings*
