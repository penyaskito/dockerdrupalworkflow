# DockerDrupalWorkflow, a docker development environment focused on real workflows 

Forked from [Docker4Drupal repository](https://github.com/wodby/docker4drupal)
Created by [Pepesan](https://www.linkedin.com/in/davidvaquero/)

# Introduction
This project comes to resolve some issues when we need to develop a drupal site. It's based on Docker and have some improvements from the docker4drupal project:
* Easy installation: we provide a set of scripts to install all the requirements to handle te project
* Improved environment settings: the .env file define almost all the environment variables needed to manage the project 
* Included script for Drupal Installation: the install_and_launch.sh script performs a drupal installation with the default settings
  * Support for choosing the drupal theme to be installed
  * All the basic modules to develop a modern Drupal site in stable state
* Architecture Best practice included: the "basic installation" includes and configures the integration with:
  * redis
  * varnish
  * solr
  * nginx
* Latest versions: we have included and test the last version of the services and the modules
* Behat integration
* Scripts for:
  * Generating the docker images: with these scripts we can create and upload the docker images needed by the project
  * Managing the database dumps and the configuration files
  * Check the drupal status
  * Install only the actual state of the project, not all the steps from the initial development
  * and much more...
# Requirements
* Docker (at least v19.03.8)
* docker-compose (at least v1.27.4)
* composer (v2 recommended)
* drush (see .env file)
* git (last version)
* nodejs (last version)
* npm (last version)
* curl (last version)
* php  (al least 7.4 version)
  * extensions needed: curl, gd, mysqli, pdo_mysql, zip, mbstring, xml
  * Review the memory limit, max_execution_time, etc...
* mysql-client (valid for mariadb version)
* Look the available ports in docker-compose.yaml
## For all systems
* Download the repository, cloning the repo or download the zip from the repository and extract it into aa folder
* Move into the folder  
* Create a copy of the .env.example to .env
cp .env.example .env
*  Edit the .env file to change the variables if needed
## for Ubuntu 20.04
use the script:
install_requirements_ubuntu_20_04.sh
## for Centos 8
use the script:
install_requirements_centos_8.sh
## for Windows Requirements
* git bash with the Use Windows default console window setting, not the Use Mintty one 
# Development environment
## .env File
Inside the .env file should be defined the environment variables that defines the installation and the rest of the environments
### Copy the .env.example to the .env file, if is not done before
cp .env.example .env
### Edit the .env file
Edit the variables for your case if it's necessary
## Drupal first installation
./install_and_launch.sh
### Windows Install
With the Gitbash terminal execute:
./install_and_launch_windows.sh
### ¿What does?
* ./download_drupal.sh // download the drupal defined in the DRUPAL_VERSION variable
* ./create_dev_volumes.sh // create the folders needed
* docker-compose up -d  // start the environment containers
* sleep 10 // wait for the mysql start process
* ./install_drupal.sh // install drupal and dependencies
*  echo "Enter to http://localhost/" 
Note: the windows scripts are similar but with _windows postfix
## Nginx URL
[http://localhost/](http://localhost)
## VARNISH URL
[http://localhost:8080/](http://localhost:8080/)
## SOLR URL
[http://localhost:8983/](http://localhost:8983/)
## MATOMO URL
[http://localhost:8082/](http://localhost:8082/)
## Complemental ports
* redis: 6379
* app-mariadb: 3306
* php-fpm: 9002
* matomo: 8082
* matomo-mariadb: 3308
## Include your IP in the configuration
We need to include you server IP intro the drupal_etc/settings-dev.php file, this file will mount into the PHPFPM docker container into the default installation folder.
``
$settings['trusted_host_patterns'] = [
'^localhost$',
];``
into
``
$settings['trusted_host_patterns'] = [
'^localhost$',
'server_ip'
];``
## Troubleshooting
* If you have problems to start the containers, and not found the correct images, check if you have the .env file defined. If not copy, the .env.example file to .evn file. And check the variables that defines the docker image versions.
* If you have problems to start te containers because a mount problem of the settings.php file:
  * delete the drupal/www/site/default/settings.php
  * copy the drupal_etc/settings-dev.php to drupal/www/site/default/settings.php
  * remove the mount point in the app container into the docker-compose.yaml file
  * Try to restart the containers: docker-compose up -d
* If you need to restart all the environment, DELETING all the data, including the drupal folder, execute the clean_all.sh script


## Backup Drupal database
./backup-database.sql
## Stop the environment
./docker-compose down
## Start the environment
./docker-compose up -d
## Creating dev images
To create the dev images we need to execute:
./create_image.sh
Note: Don't forget to change the .env file with the name of the images and their tags
## Upload drupal code
Erase the .gitignore file line that prevents that the drupal folder could be commited and pushed into the repository:

from

``drupal ``

to

``#drupal ``

## Onboarding a new team member
If you need to deploy a new development environment with a project that is in active development we need to follow the next steps:
* download the last version of the development branch from your repository 
* Install the requirements: ./install_requirements_ubuntu_20_40.sh
* Execute the environment: ./install_actual_drupal.sh
* If you is installing is a remote/virtual machine server check teh settings-dev.php to include the server ip at the bottom of the file
* Access to the main URL: http://localhost/ or http://server_ip/
# Testing
We provide a way to manage BDD test with Behat
## Requirements on system
* php-curl
* drush on system
  * composer global require drush/drush
  * export PATH=$PATH:$HOME/.composer/vendor/bin
  * or export PATH=$PATH:$HOME/.config/composer/vendor/bin
## requirement in the container
* docker-compose exec app composer require drupal/drupal-extension -d /var/www
## usage
./run_tests.sh
## show possible steps
./drupal/vendor/bin/behat -dl
## Features location
* ./features
  * home.feature

# Production Environment
## .env File
Such the development environment, in production, we need to define some variables in the .env file, such as the Docker images for production
## BE CAREFUL, this steps overwrite the drupal database in production
## Create Docker Production images
./create_image_prod.sh
## SQL import in production
copy the sql dump file into /tmp/drupal-export.sql in the production app container (app-prod)
## Launch the production environment
./start_production.sh
### ¿What does?
* Create the production docker images
* Create the volume directories for production
* Execute: docker-compose -f docker-compose-prod.yaml up -d
* Wait 10 seconds
* Import the sql dump file intro production database
# Production Varnish URL 
[http://localhost:8080/](http://localhost:8080/)
# Production Nginx URL 
[http://localhost/](http://localhost:8080/)


