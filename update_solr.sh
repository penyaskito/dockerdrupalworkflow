#!/bin/bash
set -eux
source .env
docker-compose --env-file .env stop solr
docker-compose --env-file .env rm solr
sudo rm -rf volumes-$ENV_STAGE/solr-data
sudo mkdir -p volumes-$ENV_STAGE/solr-data
sudo chmod 777 volumes-$ENV_STAGE/solr-data
docker-compose --env-file .env up -d
docker-compose exec \
solr \
solr create_core -c drupal -d /opt/solr/server/solr/drupal
docker-compose --env-file .env exec -u www-data app drush sapi-i