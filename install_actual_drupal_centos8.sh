#!/bin/bash
source .env
docker-compose down
rm -rf ./volumes-$ENV_STAGE

./create_dev_volumes_centos8.sh
docker-compose up -d
sleep 10
cp ./drupal_etc/default.settings.php ./drupal_etc/settings-$ENV_STAGE.php
cp ./sql_dump/drupal-export-latest.sql ./volumes-$ENV_STAGE/mariadb-tmp/drupal-export.sql
./import-database.sh
# update permissions to www-data user and group
docker-compose exec -u root app chown -R $DUID:$DGID /var/www
chown -R $DUID:$DGID ./volumes-$ENV_STAGE/app-tmp
docker-compose exec -u root app composer install -d /var/www
docker-compose exec solr solr create_core -c drupal -d /opt/solr/server/solr/drupal
docker-compose exec app drush -y updatedb
docker-compose exec -u www-data app drush -y cron
docker-compose exec -u www-data app drush -y cr
docker-compose exec -u www-data \
  app drush  -y \
  pm-updatestatus

