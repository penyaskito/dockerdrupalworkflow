#!/bin/bash
source .env
set -eux
chmod 777 volumes-$ENV_STAGE
mkdir -p volumes-$ENV_STAGE/mariadb-tmp
chmod 777 volumes-$ENV_STAGE/mariadb-tmp
mkdir -p volumes-$ENV_STAGE/matomo-tmp
chmod 777 volumes-$ENV_STAGE/matomo-tmp
mkdir -p volumes-$ENV_STAGE/mariadb-matomo-tmp
chmod 777 volumes-$ENV_STAGE/mariadb-matomo-tmp
mkdir -p volumes-$ENV_STAGE/app-tmp
chmod 777 volumes-$ENV_STAGE/app-tmp
mkdir -p volumes-$ENV_STAGE/app-tmp/content_sync
chmod 777 volumes-$ENV_STAGE/app-tmp/content_sync
mkdir -p volumes-$ENV_STAGE/solr-data
chmod 777 volumes-$ENV_STAGE/solr-data