#!/bin/bash
source .env
echo "Creating NGINX Image ${DOCKER_NGINX_IMAGE}"
docker build -t $DOCKER_NGINX_IMAGE_BASE:latest -f Dockerfile.dev.nginx .
docker build -t $DOCKER_NGINX_IMAGE_BASE:$DOCKER_NGINX_IMAGE_LAST_VERSION -f Dockerfile.dev.nginx .
#    --no-cache
echo "Login in"
docker login
echo "Uploading the NGINX image to the repository"
docker push $DOCKER_NGINX_IMAGE_BASE:latest
docker push $DOCKER_NGINX_IMAGE_BASE:$DOCKER_NGINX_IMAGE_LAST_VERSION