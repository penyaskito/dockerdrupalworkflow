#!/bin/bash
set -eux
docker-compose exec -u www-data \
  app  \
  composer show -d /var/www
docker-compose exec -u www-data \
  app  \
  composer status -d /var/www -v
docker-compose exec \
  app  \
  composer self-update -d /var/www

docker-compose exec -u www-data \
  app  \
  composer diagnose -d /var/www
docker-compose exec -u www-data \
  app  \
  composer show drupal/core-recommended -d /var/www
docker-compose exec -u www-data \
  app  \
  drush pm:list
docker-compose exec -u www-data \
  app  \
  composer outdated "drupal/*" -d /var/www


