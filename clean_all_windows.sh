#!/bin/bash
source .env
docker-compose down
docker images prune -a
docker volume prune -f
rm -rf volumes-$ENV_STAGE ./drupal
rm -rf drupal_etc/settings*
