#!/bin/bash
# debug
#set -o xtrace
source .env
echo "Volcando la BBDD"
export TIMESTAMP=$(date +"%Y-%m-%d-%H-%M-%S-%N")
docker-compose exec -e TIMESTAMP=$TIMESTAMP app //bin/bash -c 'drush sql:dump > "/tmp/drupal-export-${TIMESTAMP}.sql"'
docker-compose exec -e TIMESTAMP=$TIMESTAMP app //bin/bash -c 'drush sql:dump > "/tmp/drupal-export-latest.sql"'
cp ./volumes-dev/app-tmp/drupal-export* ./sql_dump

