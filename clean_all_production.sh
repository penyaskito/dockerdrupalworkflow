#!/bin/bash

./stop_production.sh
docker images prune -a
docker volume prune -f
sudo rm -rf volumes-prod
