#!/bin/bash
set -eux
mkdir -p volumes-prod
chmod 777 volumes-prod
mkdir -p volumes-prod/mariadb-tmp
chmod 777 volumes-prod/mariadb-tmp
mkdir -p volumes-prod/app-tmp
chmod 777 volumes-prod/app-tmp
mkdir -p volumes-prod/logs
chmod 777 volumes-prod/logs
mkdir -p volumes-prod/solr-data
chmod 777 volumes-prod/solr-data
docker-compose -f docker-compose-prod.yaml up -d
sleep 20
./install_production_windows.sh
chmod -R 777 drupal_etc/drupal_public/
chmod -R 777 drupal_etc/drupal_private/
mkdir -p ./volumes-prod/app-tmp/import
chmod -R 777 ./volumes-prod/app-tmp/import
cp -r ./drupal_etc/drupal_sync_prod/* ./volumes-prod/app-tmp/import
docker-compose -f docker-compose-prod.yaml exec -u www-data app-prod drush  -y config-import --partial --source /tmp/import

docker-compose -f docker-compose-prod.yaml exec -u www-data app-prod drush  -y locale-check
docker-compose -f docker-compose-prod.yaml exec -u www-data app-prod drush  -y locale-update
docker-compose -f docker-compose-prod.yaml exec -u www-data app-prod drush  -y updatedb-status

docker-compose -f docker-compose-prod.yaml exec -u www-data app-prod drush  -y cron

docker-compose -f docker-compose-prod.yaml exec -u www-data app-prod drush  -y cache-rebuild

